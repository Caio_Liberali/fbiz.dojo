describe("Jokenpo", function() {
  var jokenpo = require('../lib/jokenpo.js');
  it("Pedra + pedra dão empate", function() {
    expect(jokenpo("pedra", "pedra")).toEqual("empate");
  });

  it("tesoura + tesoura = empate", function() {
    expect(jokenpo("tesoura", "tesoura")).toEqual("empate");
  });

  it("papel + papel = empate", function() {
    expect(jokenpo("papel", "papel")).toEqual("empate");
  });

  it("Pedra + tesoura = pedra", function() {
    expect(jokenpo("pedra", "tesoura")).toEqual("pedra");
  });

  it("Tesoura + pedra = pedra", function() {
    expect(jokenpo("tesoura", "pedra")).toEqual("pedra");
  });

  it("Tesoura + papel = tesoura", function() {
    expect(jokenpo("tesoura", "papel")).toEqual("tesoura");
  });

  it("Papel + tesoura = tesoura", function() {
    expect(jokenpo("papel", "tesoura")).toEqual("tesoura");
  });

  it("papel + pedra = papel", function() {
    expect(jokenpo("papel", "pedra")).toEqual("papel");
  });

  it("groselha + pedra = erro", function() {
    expect(jokenpo("groselha", "pedra")).toEqual("erro");
  });

  it("pedra + groselha = erro", function() {
    expect(jokenpo("pedra", "groselha")).toEqual("erro");
  });

  it("pedra + nada = erro", function() {
    expect(jokenpo("pedra")).toEqual("erro");
    expect(jokenpo("pedra",null)).toEqual("erro");
  });

    it("pedra + papel + outra coisa = erro", function() {
    expect(jokenpo("pedra", "papel", "outra coisa")).toEqual("erro");
  });

  it("numero + numero = erro", function() {
    expect(jokenpo(2, 1)).toEqual("erro");

    expect(jokenpo("pedra", 1)).toEqual("erro");
    expect(jokenpo(2, "pedra")).toEqual("erro");
  });

  it("pedra + papel = papel", function() {
    expect(jokenpo("pedra", "papel")).toEqual("papel");
    expect(jokenpo("Pedra", "papel")).toEqual("papel");
    expect(jokenpo("Pedra ", "papel")).toEqual("papel");
  });





});
