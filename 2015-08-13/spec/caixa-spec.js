describe('Caixa eletrônico', function(){

	var caixa = require('../lib/caixa-refactory-aragao.js');

	it('quando saque for 10, 20, 50, 100', function() {
		expect(caixa(10)).toBe("10");
		expect(caixa(20)).toBe("20");
		expect(caixa(50)).toBe("50");
		expect(caixa(100)).toBe("100");
	});

	it('quando saque for 30', function() {
		expect(caixa(30)).toBe("20-10");
	});

	it('quando saque for 40', function() {
		expect(caixa(40)).toBe("20-20");
	});

	it('quando saque for 80', function() {
		expect(caixa(80)).toBe("50-20-10");
	});

	it('quando saque for 130', function() {
		expect(caixa(130)).toBe("100-20-10");
	});

	it('quando saque for 530', function() {
		expect(caixa(530)).toBe("100-100-100-100-100-20-10");
	});

	it('quando saque for 25', function() {
		expect(caixa(25)).toBe("erro");
	});

	it('quando saque for 0', function() {
		expect(caixa(0)).toBe("");
	});

});
