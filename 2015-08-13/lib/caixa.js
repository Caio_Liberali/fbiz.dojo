module.exports = function(valor) {

	var resto,
		resultado = [],
		notas = [100,50,20,10];

	//forca variavel para numero inteiro
	valor = parseInt(valor);

	if(notas.indexOf(valor) >= 0 ){
		return valor.toString();
	}

	function notasRepetidas (valorNota) {
		var divisao = Math.floor(valor/valorNota);
		for (var i = 0; i < divisao; i++) {
			resultado.push(valorNota);
			valor = valor - valorNota;
		}
	}

	notas.forEach(function(valorNota){
		resto = valor % valorNota;

		if(resto < valor){
			notasRepetidas(valorNota);
		}
	});

	if(valor !== 0){
		return "erro";
	}

	return resultado.join("-");
};
