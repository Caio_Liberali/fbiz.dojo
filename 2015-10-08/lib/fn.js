module.exports = function(valor) {

	var numeros = [
		undefined,
		[" ", "|", "|"].join('\n'),
		[" _ ", " _|", "|_ "].join('\n'),
		["_ ", "_|", "_|"].join('\n'),
		["   ", "|_|", "  |"].join('\n'),
		[" _ ", "|_ ", " _|"].join('\n'),
		[" _ ", "|_ ", "|_|"].join('\n'),
		["_ ", " |", " |"].join('\n'),
		[" _ ", "|_|", "|_|"].join('\n'),
		[" _ ", "|_|", " _|"].join('\n')
	];
	
	return {
		numeros: numeros,
		
		getCompoundNumber: function(valor) {
			valor = valor.split('\n');
			var colunas_vazias = this.getEmptyColumns(valor);
			var numeros_array = this.getNumbersArray(valor, colunas_vazias);
			var valor_numeros = "";			
			for (var index = 0; index < numeros_array.length; index++) {
				var retorno = this.getNumber(numeros_array[index]);
				if (retorno == false) {
					return retorno;
				}
				valor_numeros = valor_numeros + retorno;
			}				
			return parseInt(valor_numeros);
		},
		
		getNumber: function(valor) {
			if (valor.length > 11) {
				return this.getCompoundNumber(valor);
			}
			var index = numeros.indexOf(valor);
			return index < 0 ? false : index;
		},
		
		getEmptyColumns: function(valor) {
			var colunas_array = [];
			for	(var col = 0; col < valor[0].length; col++) {
				var coluna_vazia = true;				
				for (var row = 0; row < valor.length; row++) {
					if (valor[row][col] != ' ') {
						coluna_vazia = false;
					}						
				}								
				if (coluna_vazia) {
					colunas_array.push(col);
				}
			}
			return colunas_array;
		},
		
		getNumbersArray: function(valor, colunasVazias) {
			var numeros_composto_array = [];
			for (var col = 0; col < colunasVazias.length; col++) {
				var numeros_array = [];
				if (col + 1 < colunasVazias.length && col + 1 != colunasVazias[col + 1]) {
					var numero_array_linha = "";
					for (var rowValor = 0; rowValor < valor.length - 1; rowValor++) {
						numero_array_linha = numero_array_linha + (rowValor > 0 ? '\n' : '');
						for (var colValor = colunasVazias[col] + 1; colValor < colunasVazias[col + 1]; colValor++) {
							numero_array_linha = numero_array_linha + valor[rowValor][colValor];
						}
					}
					numeros_array.push(numero_array_linha);
					numeros_composto_array.push(numero_array_linha);
				}
			}
			return numeros_composto_array;
		}
		
	};
};
