describe('Teste dos Números', function() {

	var fn = require('../lib/fn.js')();
	var numeros = [
		undefined,
		[" ", "|", "|"].join('\n'),
		[" _ ", " _|", "|_ "].join('\n'),
		["_ ", "_|", "_|"].join('\n'),
		["   ", "|_|", "  |"].join('\n'),
		[" _ ", "|_ ", " _|"].join('\n'),
		[" _ ", "|_ ", "|_|"].join('\n'),
		["_ ", " |", " |"].join('\n'),
		[" _ ", "|_|", "|_|"].join('\n'),
		[" _ ", "|_|", " _|"].join('\n')
	];
	var numero_composto = [
		'     _  ',
		'  |  _| ',
		'  | |_  ',
		'        '
	].join('\n')


	it('Teste dos Números' , function() {
		expect( fn.getNumber(numeros[1]) ).toBe(1);
		expect( fn.getNumber(numeros[2]) ).toBe(2);
		expect( fn.getNumber(numeros[3]) ).toBe(3);
		expect( fn.getNumber(numeros[4]) ).toBe(4);
		expect( fn.getNumber(numeros[5]) ).toBe(5);
		expect( fn.getNumber(numeros[6]) ).toBe(6);
		expect( fn.getNumber(numeros[7]) ).toBe(7);
		expect( fn.getNumber(numeros[8]) ).toBe(8);
		expect( fn.getNumber(numeros[9]) ).toBe(9);
	});

	it('Teste dos Números Composto' , function() {
		expect( fn.getNumber(numero_composto)).toBe(12);
	});
	
	it('Teste de Falha' , function() {
		expect( fn.getNumber("") ).toBe(false);
	});

});
