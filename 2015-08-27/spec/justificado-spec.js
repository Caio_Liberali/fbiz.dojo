describe('Justificar uma string com um numero fixo de colunas', function(){

	var pauta = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eu laoreet tellus, pretium viverra turpis. Phasellus tristique egestas augue sit amet venenatis. Donec in porttitor dolor, sit amet congue urna. Nunc elit sapien, varius eget odio tristique, ullamcorper interdum nibh. Donec aliquam velit ac malesuada sodales. Proin semper vulputate metus vel cursus. Nunc rutrum lacinia sem, vel pretium sem cursus id. Aliquam commodo, ante eget mollis iaculis, felis ex mollis odio, nec interdum metus lorem quis dui. Maecenas pharetra viverra aliquet. In non tellus pretium, dignissim lectus quis, imperdiet sem. Quisque hendrerit tristique diam.";
	var com2linhas = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
	var linha1 = "Lorem ipsum dolor sit amet,";

	var justificado = require('../lib/justificado.js');

	it('verificando primeira linha', function() {
		expect(justificado(linha1, 30))
			.toBe("Lorem  ipsum  dolor  sit amet,");
	});

	it('verificando quando tiver mais de uma linha', function() {
		expect(justificado(com2linhas, 30))
			.toBe("Lorem  ipsum  dolor  sit amet,\nconsectetur  adipiscing  elit.");
	});
});
