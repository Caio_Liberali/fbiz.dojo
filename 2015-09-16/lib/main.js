var args = process.argv.slice(2);
var turnTime = args[0];
var totalTurn = args[1];
var timer = require('./fn')(turnTime, totalTurn);

setInterval(function () {
    if (!timer.tick()) {
        process.exit(0);
    }
}, 1000);
