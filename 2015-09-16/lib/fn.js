module.exports = function (turnTime, totalTurn) {

    turnTime = turnTime * 1000;

    var exec = require('child_process').exec;
    var counter = 0;
    var currentTurn = 0;
    var turnInterval = 10 * 1000;
    var gameStart = new Date();
    var turnStart = gameStart;
    var total = Number(gameStart) + (totalTurn * turnTime) + (turnInterval * (totalTurn - 1));

    return {
        isTurn: function (start, end) {
            return ((Number(end) - Number(start)) == turnTime);
        },
        isEndOfGame: function (turno) {
            return turno >= totalTurn;
        },
        tick: function () {
            var now = Number(gameStart) + (++counter * 1000);
            process.stdout.write('\033c');
            console.info(this.showTimeUTC(new Date(total - now)));

            if (this.isTurn(turnStart, now)) {
                turnStart = now + turnInterval;
                currentTurn++;

                if (this.isEndOfGame(currentTurn)) {
                    exec('say -v Joana "Então pronto! Acabaste o jogo!"');
                    console.log("Então pronto! Acabaste o jogo!");
                    return false;
                }

                exec('say -v Joana "Acabou a rodada, troquem as cadeiras pois!"');
                console.log("Acabou a rodada, troquem as cadeiras pois!");
            }
            return true;
        },
        getTotalTurns: function () {
            return currentTurn;
        },
        showTime: function (time) {
            return [
				this.leadingZero(time.getHours()),
				this.leadingZero(time.getMinutes()),
				this.leadingZero(time.getSeconds())
            ].join(':');
        },
        showTimeUTC: function (time) {
            return [
				this.leadingZero(time.getUTCHours()),
				this.leadingZero(time.getUTCMinutes()),
				this.leadingZero(time.getUTCSeconds())
            ].join(':');
        },
        leadingZero: function (val) {
            return val < 10 ? '0' + val : val;
        }
    }
};
