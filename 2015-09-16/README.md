# Dojo - 16/09/2015

Dojo realizado na F.biz, feito em JS.

## Problema: Criação de um timer para Coding Dojos

A idéia é criar um timer que de X em X minutos fale alguma coisa indicando que a rodada acabou — com a voz do Stephen Hawkins.

Além disso, após X números de rodadas ele fale alguma outra coisa indicando o fim do jogo.

Como o timer do dojo precisa considerar o tempo das pessoas mudarem de cadeira, precisamos criar um intervalo de 10 segundos (não contabilizados) entre cada rodada.

Mostrar quando tempo falta para acabar a rodada atual e quanto tempo falta para acabar o dojo.

### Regras

* Usuário define o tempo de uma rodada **em minutos** (número decimal mesmo);
* Usuário define o total de rodadas (apenas números inteiros);
* Usuário define a frase de fim de rodada;
* Usuário define a frase de fim de jogo;
* Cada final de rodada o Stephen Hawkins noiadão fala uma barbaridade;
* Cada final de rodada tem 10 segundos para começar a próxima rodada;
  * a última rodada não tem 10 segundos extras;
* Quando chegar no final da última rodada, Stephen Hawkins fala algo pra fechar o jogo;
* Mostrar quanto tempo falta para acabar a rodada atual no formato `MM:SS`;
  * Enquanto estiver no intervalo de 10 segundos, o contador de rodada fica zerado;
* Mostrar quanto tempo falta para acabar o dojo no formato `HH:MM:SS`;

### Exemplo

    dojoTimer(5, 10,
      "Change now! Remember, I'm a robot and I can kill you.",
      "Now you will all die! ha ha ha");
    // roda por 51.5 minutos, e mostra no começo "00:51:30 / 05:00"
    // com 10 rodadas de 5 minutos e
    // intervalo de 10 segundos,
    // falando "Change now! Remember..." a cada rodada e
    // "Now you will..." no final do jogo.

_No mac, pra fazer o computador falar alguma coisa é só usar o comando_ `say "[frase]"` _(dica quente do @hankpillow)._

Retirado do [disscourse](http://discourse.fbiz.com.br/t/evento-5-dojo-na-f-biz-27-08/434/2)

## Participantes

* Caio Liberali
* Igor Almeida
* Marcelo Miranda Carneiro
* Renato Inamine

## :)

* Conseguimos concluir o dojo;
* Tipo de problema com implementação de TDD menos óbvia;
* Foram abordados conceitos de escopo;
* Tipagem no javascript (string + number, string * number, etc.);
* Como tinha pouca gente, deu pra todo mundo participar muitas vezes;
* 2 rodadas apenas de análise do problema;
* Saímos do padrão e problema é útil para o próprio dojo;

## :(

* Poucas pessoas;
* Faltou café;

## Melhorar

* Deixar o Dojo de 2 em 2 semanas pra ver se as pessoas vão mais;
