describe('Título do Teste:', function () {

    var fn = require('../lib/fn.js');
    var now = new Date();

    it('Final de rodada', function () {
        var timer = fn(10);
        expect(timer.isTurn(now - 10 * 1000, now))
			.toBe(true);
    });

    it('Não é final de rodada', function () {
        var timer = fn(10);
        expect(timer.isTurn(now - 10 * 1000, now - 1000))
			.toBe(false);
        expect(timer.isTurn(now - 10 * 1000, now + 1000))
			.toBe(false);
    });

    it('Final de jogo', function () {
        var timer = fn(10, 5);
        expect(timer.isEndOfGame(1))
			.toBe(false);
        expect(timer.isEndOfGame(5))
			.toBe(true);
    });

    it('Final de jogo apos 5 rodadas', function () {
        var timer = fn(10, 5);
        for (var i = 1; i <= 100; i++) {
            timer.tick();
        }
        expect(timer.isEndOfGame(timer.getTotalTurns()))
			.toBe(true);
    });

    it('Exibir relógio de forma legivel', function () {
        var timer = fn(10, 5);
        expect(timer.showTime(new Date(2015, 00, 01, 01, 00, 00)))
            .toBe("01:00:00");
    });

    it('Exibir relógio UTC ( Brasil +3 ) de forma legivel', function () {
        var timer = fn(10, 5);
        expect(timer.showTimeUTC(new Date(2015, 00, 01, 01, 00, 00)))
            .toBe("03:00:00");
    });

});
