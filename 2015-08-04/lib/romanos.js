module.exports = function(valor) {

	var retorno;

	function toThree(v){

		if(v <= 3){
			var resultado = "";
			for(var i = 0; i < v; i++)
			{
				resultado = resultado + "I";
			}
			return resultado;
		}
	}
	if(15 % valor === 1){
		retorno = "X" + toThree(15 % valor) + "V";
	}

	if(valor%10 <= 3){
		retorno = "X" + toThree(valor % 10);
	}

	if (valor % 10 === 0){
		retorno = "X";
	}

	if (10 % valor == 1){
		retorno = toThree(10 % valor) + "X";
	}

	if( valor < 10 && valor % 5 == 0 ){
		console.log("v", valor);
		retorno = "V";
	}

	if (valor < 10 && 5 % valor == 1){
		console.log("iv", valor);
		retorno = toThree(5 % valor) + "V";
	}

	if(valor < 10 && valor % 5 <= 3){
		retorno = "V" + toThree(valor % 5);
	}

	if(valor <=3){
		retorno = toThree(valor);
	}

	return retorno;
};
