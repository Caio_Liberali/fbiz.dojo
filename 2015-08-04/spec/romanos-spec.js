describe('Números romanos', function(){

	var romanos = require('../lib/romanos.js');

	it('testando até 3', function() {
		expect(romanos(1)).toBe('I');
		expect(romanos(2)).toBe('II');
		expect(romanos(3)).toBe('III');
	});

	it('testando numero 4,9 e 14', function() {
		expect(romanos(4)).toBe('IV');
		expect(romanos(9)).toBe('IX');
		expect(romanos(14)).toBe('XIV');
	});

	it('testando numero 5', function() {
		expect(romanos(5)).toBe('V');
	});

	it('testando de 6 até 8', function() {
		expect(romanos(6)).toBe('VI');
		expect(romanos(7)).toBe('VII');
		expect(romanos(8)).toBe('VIII');
	});

	it('testando numero 10', function() {
		expect(romanos(10)).toBe('X');
	});

	it('testando numero 11 até 13', function() {
		expect(romanos(11)).toBe('XI');
		expect(romanos(12)).toBe('XII');
		expect(romanos(13)).toBe('XIII');
	});

});
